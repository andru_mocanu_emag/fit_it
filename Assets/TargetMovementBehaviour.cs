﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TargetMovementBehaviour : MonoBehaviour {

    public Button btn;

    public float xOffset;
    public float zOffset;

    public float rotation;

	// Use this for initialization
	void Start () {
        btn = GetComponent<Button>();

        btn.onClick.AddListener(() => { doOffset(); });
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    public void doOffset()
    {
        ObjectManager.selected.transform.position += new Vector3(xOffset, 0, zOffset);
        ObjectManager.selected.transform.localEulerAngles += new Vector3(0, 0, rotation);

        if(xOffset == 0 && zOffset == 0 && rotation == 0)
        {
            Destroy(ObjectManager.selected.transform.gameObject);
            print(ObjectManager.selected);
        }
    }
}
