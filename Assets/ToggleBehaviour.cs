﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ToggleBehaviour : MonoBehaviour {

    public Button btn;
    public bool toggled = true;
    public GameObject target;

    // Use this for initialization
    void Start()
    {
        btn = GetComponent<Button>();

        btn.onClick.AddListener(() => { doToggle(); });
    }

    // Update is called once per frame
    void Update () {
        
	}

    public void doToggle()
    {
        if (toggled)
        {
            target.transform.localScale = new Vector3(0, 0, 0);
            toggled = false;
        }
        else
        {
            toggled = true;
            target.transform.localScale = new Vector3(1, 1, 1);
        }
    }
}
