﻿using UnityEngine;
using System.Collections;

public class ObjectManager : MonoBehaviour {

    private Ray ray;
    public static HoverableBehaviour selected = null;

	// Use this for initialization
	void Start () {
	    
	}
	
	// Update is called once per frame
	void Update () {
	    if (Input.GetMouseButtonDown(0))
        {
            
            ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit))
            {
                HoverableBehaviour hoverable = hit.transform.GetComponent<HoverableBehaviour>();

                if (hoverable != null)
                {
                    selected = hoverable;
                    //do highlight
                }
            }
        }
	}
}
