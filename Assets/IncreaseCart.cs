﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class IncreaseCart : MonoBehaviour {

    public Button btn;

	// Use this for initialization
	void Start () {
        btn = GetComponent<Button>();

        btn.onClick.AddListener(() => { doIncrease(); });
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void doIncrease ()
    {
        CartAmount.amount++;
    }
}
