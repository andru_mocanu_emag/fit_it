﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CartAmount : MonoBehaviour {

    public static int amount = 0;
    public Text label;

	// Use this for initialization
	void Start () {
        label = GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {
        label.text = "" + amount;
	}
}
