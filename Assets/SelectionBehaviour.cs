﻿using UnityEngine;
using UnityEngine.UI;

using System.Collections;
using System.Collections.Generic;

public class SelectionBehaviour : MonoBehaviour {

    public Button btn;

	// Use this for initialization
	void Start () {
        btn = GetComponent<Button>();

        btn.onClick.AddListener(() => { doSelect(); });
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void doSelect () {
        GameObject model = btn.transform.parent.parent.FindChild("Model").GetChild(0).gameObject;
        GameObject newModel = Instantiate(model);
        newModel.AddComponent<HoverableBehaviour>();

        GameObject target = GameObject.Find("Objects");
        
        newModel.transform.parent = target.transform;
        newModel.transform.position = new Vector3(0, 48, 0);
        print(newModel.transform.name);
        if(newModel.transform.name.Contains("Vaza"))
        {
            newModel.transform.localScale += new Vector3(4, 4, 4);
        }
        else
        {
            newModel.transform.localScale += new Vector3(500, 500, 500);
        }

        ObjectManager.selected = newModel.GetComponent<HoverableBehaviour>();
        
    }

    private void killChildren(Transform parent)
    {
        List<GameObject> children = new List<GameObject>();
        foreach (Transform child in parent.transform) children.Add(child.gameObject);
        children.ForEach(child => Destroy(child));
    }
}
